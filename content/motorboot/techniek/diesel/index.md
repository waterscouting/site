---
title: Diesel
type: docs
---

Het is uiteraard belangrijk om te controleren of er voldoende diesel in de tank zit voordat je gaat vertrekken. 

### Diesel tanken
Het tanken van een boot is vergelijkbaar met het tanken van een auto. Toch zijn er enkele aandachtspunten. Zo bevind het dieselgat zich op een boot vaak horizontaal. Houd de tankspuit omhoog bij het aanpakken en teruggeven, zo loopt er niets uit op de kant of op de boot. Tank de boot ook niet te vol, er moet in de tank een beetje ruimte overblijven omdat de diesel uitzet. Als er dan geen ruimte over is kan diesel via de tankontluchting er uit worden gedrukt.

### Ontluchten
Nadat de dieselslangen van het motorblok los zijn geweest, of je bijvoorbeeld stil bent gevallen met een lege tank zit er lucht in de leidingen. Sommige motorblokken ontluchten de dieseltoevoer zelf, bij andere moet je met een handmatig opvoerpompje brandstof naar de motor pompen. Op het fijnfilter zit vaak een aftappunt, deze moet tijdens het opvoeren dan open staan. Sluit het aftappunt weer als er alleen maar schone brandstof uitkomt (dus geen lucht meer).
