---
title: Motorolie
type: docs
---

Een motorblok bevat olie in de hoofdzaak om te smeren. Doordat er olie in het blok stroomt, ontstaat er tussen onderdelen een oliefilm. Deze oliefilm zorgt ervoor dat de onderdelen elkaar net niet raken. Met te weinig of geen olie worden deze onderdelen dus niet gesmeerd, waardoor snel slijtage optreed.

{{< alert >}}Een voldoende oliepeil is dus van levensbelang voor de motor! Reden om deze bij voorkeur voor ieder vertrek te controleren.{{< /alert >}}

### Olie peilen
Het peilen van de olie gebeurt doormiddel van een peilstok. Haal deze uit het blok, en haal deze dan langs een doek zodat hij vrij van olie is. Vervolgens stop de je peilstok terug, helemaal tot hij in het blok zit. Als je nu de peilstok er weer uit haalt, kan je aan de onderkant van de peilstok het oliepeil aflezen.

Dit werkt als volgt: Op de peilstok staan twee streepjes, het onderste streepje geeft het minimumpeil weer, en het bovenste streepje het maximumpeil. Zolang het oliepeil tussen deze twee streepjes staat is het oliepeil dus in orde.

### Te weinig olie
Bij te weinig olie kan de motor snel verslijten zoals hierboven beschreven. Dan moet er dus olie worden bijgevuld tot het minimum peil. Dat doe je als volgt:
1. Controleer welke oliesoort er in het motorblok hoort. Op de fles staan twee getallen bijvoorbeeld 5W15.
2. Vul niet te veel in 1 keer bij. Liever wat vaker peilen dan dat er al te veel in zit. Als het oliepeil onder het minimum is 500 mL een goed startpunt.


### Te veel olie
Een motorblok mag ook nooit te veel olie bevatten! Bij te veel olie is het risico dat het motorblok op de olie gaat lopen, met andere worden: het motorblok staat op hol. Tap olie van het motorblok af totdat weer een voldoende oliepeil ontstaat. Dit kan door de aftapplug onderin het carter.

### Oliedruk
De olie in de motor staat onder een bepaalde druk. Deze druk zorgt ervoord dat overal voldoende olie beschikbaar is. Deze oliedruk wordt ook gemeten door blok. Bij onvoldoende oliedruk klinkt er een alarm.
