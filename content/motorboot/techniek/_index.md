---
title: "Techniek"
linkTitle: "Techniek"
type: docs
weight: 2
description: >
  Hoe werkt de motor en de andere technische systemen in een motorboot?
---
