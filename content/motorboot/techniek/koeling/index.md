---
title: Koeling
type: docs
---

De meeste motorblokken hebben een optimale werktemperatuur van rond de 80 graden Celcius. Een te heet motorblok kan schade veroorzaken. Het is dus zaak dat het motorblok niet te warm wordt.

### Koelvloeistof
Voordat je gaat varen is het van belang te controleren of er genoeg koelvloeistof in het motorblok zit. Hiervoor zit een dop bovenop het motorblok. Doe dit alleen als het motorblok is afgekoeld! De koelvloeistof zet uit bij warmte waardoor druk ontstaat. Bij het opendraaien van het koelreservoir van een hete motor kan stoom ontsnappen wat brandworden kan veroorzaken. Moet je toch de koelvloeistof van een hete motor controleren gebruik dan (dikke) doek om de dop te openen, en doe dit stukje voor stukje zodat de stoom kan ontsnappen.

Wat een voldoende niveau van de koelvloeistof is kan verschillen. Bij kleinere motorblokken moet het reservoir bijna helemaal gevuld zijn, maar bij grotere blokken moet meer ruimte overblijven omdat de koelvloeistof uitzet.

### Bijvullen
Te weinig koelvloeistof in het reservoir? Vul deze dan bij. Gebruik geen water om een tekort aan te vullen, tenzij er sprake is van een noodgeval en er echt geen andere mogelijkheid is. Vervang in dat geval alle koelvloeistof daarna zo snel mogelijk.
