---
title: Schroefas
type: docs
---

De schroefas loopt door een koker, die gevuld is met vet. Het vet zorgt ervoor dat er geen water de boot in kan komen, en tegelijkertijd voor de smering van de schroefas. De voorkant van de koker is afgedicht met een stopbuspakking met vetkoord. Deze afdichting zorgt ervoor dat het vet in de koker blijft.

### Schroefas smeren
Voordat je gaat varen moet je zorgen dat er voldoende vers vet in de koker zit. Aan de voorkant van de koker zit vaak een vetnippel waar je door middel van een vetspuit vet in kan drukken. Dit vet kan niet verder naar voren de koker in, daar zit immers de afdichting. De koker is aan de achterkant open, waardoor gebruikt vet in het water terecht komt. Het vet wordt dus constant van voor naar achteren gedrukt en vervangen.


