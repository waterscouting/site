---
title: Schroefeffect
type: docs
description: >
  Hoe werkt een schroef en hoe gebruik je dit in je voordeel?
---

Er zijn twee soorten schroeven, linkse schroeven en rechtse schroeven. De draairichting van een schroef geeft aan of deze een linkse of rechtse is: als een schip vooruit vaart en de schroef draait rechtsom dan is er sprake van een rechtse schroef. Als hetzelfde schip achteruit vaart dan draait de schroef linksom, maar we spreken dan nog wel van een rechtse schroef.

Als een schip vooruit vaart, stuwt de schroef water naar achteren, en dit water duwt terug. Daardoor wordt het schip vooruit geduwd, maar er wordt ook een klein deel van het water opzij geduwd. Bij een rechtse schroef wordt er meer water naar links geduwd dan naar rechts. Daardoor wordt het achterschip een klein beetje naar rechts geduwd en stuurt het schip (als er geen roer zou zijn) een beetje naar bakboord. Dit heb je alleen niet door, omdat je hier automatisch voor compenseert met het roer.

Als dit schip achteruit vaart werkt dit precies andersom. Bij een rechtse schroef wordt er dan meer water naar rechts geduwd dan naar links. Hierdoor wordt het achterschip naar bakboord geduwd. Het schroefeffect is in achteruit veel sterker dan in vooruit. Dit komt deels doordat het roer achteruit minder goed werkt (vanwege het ontbreken van roerdruk) en doordat de schroef geoptimaliseerd is voor vooruit varen.

Dit schroefeffect kunnen we gebruiken in ons voordeel. Door hier bijvoorbeeld rekening mee te houden bij het kiezen van een boeg waarover het schip wordt aangelegd. Bij het aanleggen is het bijvoorbeeld handig als het achterschip naar de kant toe trekt. Dit moet dan gebeuren bij het achteruit varen, want dan is het schroefeffect het sterkt. Bij een rechtse schroef trekt het achterschip bij het achteruit varen naar bakboord, het is dan dus bij een rechtse schroef handiger om aan te leggen aan de bakboordzijde.

{{< alert >}}
Zoek uit of het schip waarin je vaart een linkse of rechtse schroef heeft! Die informatie kan je dan gebruiken bij het manoeuvreren.
{{< /alert >}}
