---
title: Keren
type: docs
description: >
  Hoe keer je een motorboot om door middel van schroefeffect?
---

Bij het keren gaat het er om het schip 180º te draaien. Net als bij andere manoeuvres is het hierbij belangrijk dat je andere schepen niet hindert. Bij het keren is het handig om gebruik te maken van het schroefeffect, dan is de draaicirkel immers het kleinst. Zie ook [Schroefeffect](../schroefeffect).

### Keren op ruim bestek

Ruim bestek wilt zeggen dat er voldoende ruimte is om 180º te draaien zonder achteruit te hoeven varen. Zorg allereerst dat de vaart uit het schip is, met minder snelheid is de draaicirkel kleiner.

- Als het schip voorzien is van een **rechtse schroef**, dan draait het schip makkelijker over bakboord. Start dan aan de **stuurboordzijde** van het vaarwater, en draai over bakboord. Zo is de draaicirkel het kleinst.

- Als het schip voorzien is van een **linkse schroef**, dan draait het schip makkelijker over stuurboord. Start dan aan de **bakboordzijde** van het vaarwater, en draai over stuurboord. Zo is de draaicirkel het kleinst. Dit betekend dat je in feite aan de "verkeerde" kant van het water vaart. Dat mag, maar zorg wel dat je andere schepen niet hindert.

### Keren op kort bestek

Kort bestek wilt zeggen dat er niet voldoende ruimte is om 180º te draaien zonder achteruit te hoeven varen. Het is dan nodig om 1 of meerdere keren vooruit en achteruit te varen tot je helemaal gekeerd bent. Ook bij keren op kort bestek gebruik je de [Schroefeffect](../schroefeffect), maar de schroefwerking is het sterktst als je achteruit vaart, vandaar dat de manoeuvre aan de andere kant van het water begint.

**Schip met een rechtse schroef**:
1. Start aan de bakboordzijde van het vaarwater
2. Haal de vaart uit het schip, en geef dan stuurboord roer in vooruit
3. Zodra het schip bijna bij de stuurboordoever is, geef bakboord roer in achteruit
4. Zodra het schip bijna bij de bakboordoever is, geef dan weer stuurboord roer in vooruit
5. Herhaal eventueel stap 3. en 4. totdat het keren voltooid is

**Schip met een linkse schroef**:
1. Start aan de stuurboordzijde van het vaarwater
2. Haal de vaart uit het schip, en geef dan bakboord roer in vooruit
3. Zodra het schip bijna bij de bakboordoever is, geef stuurboord roer in achteruit
4. Zodra het schip bijna bij de stuurboordoever is, geef dan weer bakboord roer in vooruit
5. Herhaal eventueel stap 3. en 4. totdat het keren voltooid is

{{< alert >}}
Als het nodig is om meer dan 1 keer te steken in het vaarwater kan het makkelijk zijn om het roer in de stand te laten staan die nodig is bij vooruit varen. Dan draai je het roer niet als je achteruit vaart. De schroefwerking zorgt namelijk, afhankelijk van het schip, ook voor het draaien.
{{< /alert >}}
