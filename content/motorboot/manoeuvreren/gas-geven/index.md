---
title: Gas geven en schakelen
type: docs
description: >
  Hoe bedien je de gashendel?
---

De motor wordt bediend met een gashendel, die zowel de snelheid regelt als de vaarrichting. Bij het starten moet de gashendel in **neutraal** staan. Dit zorgt er voor dat de motor ontkoppeld is van de schroef. Dit is bij de meeste gashendels 'rechtop'.

Door de gashendel naar voren te drukken tot een 'klik' staat het schip in **stationair vooruit**. De motor is dan gekoppeld aan de schroef, en de motor loopt stationair. Dat betekend dat er net zoveel gas wordt gegeven dat de motor (net) niet uitvalt. Door de gashendel verder naar voren te drukken wordt er gas gegeven. Het schip bouwt dan snelheid op. Hoe verder de gashendel naar voren staat hoe harder de schroef draait.

Door de gashendel vanuit neutraal naar achteren te trekken tot een 'klik' staat het schip in **stationair achteruit**. Ook nu is de motor gekoppeld aan de schroef, maar dan in tegengekeerde richting door middel van de **keerkoppeling**. Ook achteruit kan je gas geven door de gashendel verder naar achteren te trekken; hoewel je achteruit normaal gesproken minder snelheid gebruikt dan vooruit.

Het is heel belangrijk om nooit de gashendel in één keer van vooruit naar achteruit te zetten. Bij bijvoorbeeld aanleggen of keren schakel je veel tussen vooruit en achteruit, waarbij je dit vaak snel wilt doen. Door dit te doen kan de motor doen afslaan en kan ernstige schade aan de keerkoppeling ontstaan. Dit komt omdat de schroef bij het terugnemen van gas nog even door blijft draaien. Mocht je in één keer door schakelen dan draaien de schroef en de motor tegengesteld aan elkaar, wat extra belasting aan de keerkoppeling geeft.

{{< alert >}}
Zet de gashendel twee tot drie seconden in neutraal voordat je schakelt tussen vooruit en achteruit.
{{< /alert >}}

Meestal is er ook een ontkoppelingsknop aanwezig bij de gashendel. Door die knop in te drukken kan er - terwijl de motor ontkoppeld is van de schroef - gas gegeven worden.
