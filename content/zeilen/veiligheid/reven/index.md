---
title: Reven
type: docs
description: >
  Hoe reef je een zeilboot?
---

Reven is het kleiner maken van het grootzeil. Reven kan nodig zijn wanneer er harde wind
is. Als het hard waait gaat de boot gemakkelijk schuin en is het moeilijker om roer te
houden en aan de wind te zeilen, er zal namelijk afgevallen moeten worden om geen water
te scheppen. Wanneer het zeil dan kleiner gemaakt wordt, kan de boot beter in bedwang
gehouden worden. Het kan natuurlijk ook zijn dat de bemanning zich niet meer veilig voelt
en dat moet voorkomen worden. Ook is het beter voor het behoud van de zeilen als er
gereefd wordt wanneer het hard waait.

Door het zeil bij de giek op te rollen, wordt er gereefd. Hoe dit precies gebeurd hoef je nog
niet te weten bij CWO I, maar wel dat het zeil kleiner gemaakt kan worden wanneer het
hard waait.
