---
title: Reddingsvest
type: docs
description: >
  Waar moet je op letten bij een reddingsvest?
---

Als je gaat zeilen is het belangrijk als er voor ieder bemanningslid een
**reddingsvest** aanwezig is. Een goed reddingsvest heeft een kraag die het
hoofd boven water houdt, voldoende **drijfvermogen** voor het gewicht
van degene die hem aanheeft en een **CE-keurmerk**, dit geeft aan dat het
reddingsvest aan de wettelijke eisen voldoet.

Er is een verschil tussen een reddingsvest en een **zwemvest**, een
zwemvest zorgt er alleen voor dat je blijft drijven. Een reddingsvest zorgt
ervoor dat je hoofd naar boven draait zodat wanneer iemand
bewusteloos is, deze toch kan blijven ademen.
