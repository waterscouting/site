---
title: Omslaan
type: docs
description: >
  Wat moet je doen als je boot omslaat?
---

Als de boot omslaat is het als eerste belangrijk om koppen te tellen en daarna altijd bij de
omgeslagen boot te blijven. Zwemmen kost namelijk veel energie, hierdoor koel je sneller af
en kan je kramp krijgen. Ook is de boot een stuk beter zichtbaar dan alleen je hoofd.
