---
title: Voorrangsregels
type: docs
description: >
  Wat zijn de voorrangregels op het water?
---

Voorrangsregels op het water zitten een beetje anders in elkaar dan op de
weg. De belangrijkste regels zijn hier beschreven.

De voorrangsregels in het BPR staat beschreven in verschillende artikelen, maar om het duidelijk
te maken hebben we ze hier op volgorde gezet. De volgorde is namelijk nogal belangrijk bij het
toepassen van deze regels, en dit werkt als volgt; als je van twee schepen wilt weten wie
er voorrang heeft, kijk je eerst of regel 1 geldig is. Als regel 1 niet van toepassing is ga je
verder met regel 2 en zo door, totdat je een regel hebt die het antwoord geeft. Als je dit
gaat oefenen is het belangrijk om niet alleen de regels te kennen, maar ze vooral ook te
kunnen toepassen, als je zelf in een boot zit! De regels zijn als volgt:

### Regel 1: Goed zeemanschap
De eerste belangrijke regel is dat **goed zeemanschap** toegepast moet worden, dit geeft aan
dat er altijd voorkomen moet worden dat:
* Mensen in gevaar komen;
* Schade wordt veroorzaakt;
* De veiligheid of "vlotte verloop van de scheepvaart" in gevaar komt.

Ook **moet** er van de voorrangsregels afgeweken worden om een aanvaring of schade te voorkomen. De
goed zeemanschap regel zegt dus eigenlijk, dat als er gevaar optreedt, dat je dan moet doen
wat het beste in dat geval is, en kijk je dus niet zo strak naar de regels. Met andere worden, als jij eigenlijk
voorrang had, maar jij had ook de aanvaring kunnen voorkomen, dan zit je ook fout.

### Regel 2: Stuurboordwal heeft voorrang
Als één van de schepen aan **stuurboordwal** vaart, heeft dat schip voorrang. Oftewel, als je als bootje netjes langs
 de rechteroever vaart, heb je vaak voorrang op alle andere boten! De stuurboordswal kan ook een boeienlijn zijn van bijvoorbeeld
 een vaargeul. In principe is al het vaarwater aan stuurboord van het midden van de vaarweg "stuurboordswal". Op sommige
 grote kanalen en rivieren mag je soms alleen maar stuurboordwal varen.

### Regel 3: Klein wijkt voor groot
Kleinere (zeil)bootjes zijn wat makkelijker te besturen dan grote (binnenvaart)schepen, en
daarom hebben die grote schepen voorrang. De grens ligt op 20 meter, is je boot kleiner
dan 20 meter, dan is het een **klein schip**, en anders is het een **groot schip**. Op welke schepen groot en klein zijn, zijn vele uitzonderingen.

### Regel 4: Zeil gaat voor spier gaat voor motor
Als het twee verschillende soorten bootjes zijn, dan is deze regel van toepassing. De regel hier
is dat **zeilboten** voor **door spierkracht voortbewogen boten** gaan, en dat deze "spier"boten weer voorrang hebben op
**motorboten**. Roeien, wrikken, bomen en jagen zijn allemaal een door spierkracht
voortbewogen boten, en die noemen we hier een "spierboot". De regel heeft te maken
met hoe makkelijk de soorten boten kunnen uitwijken! Een motorbootje kan heel makkelijk
de andere kant op sturen, terwijl zeilboten niet eens alle richtingen op kunnen varen! Heeft
een boot zowel een zeil als een motor (in gebruik)? Dan is het een motorboot.

### Regel 5: Stuurboord bij spier en motor
Zijn beide schepen gelijk, dus twee motorboten of twee roeiboten (geen zeilboten!), dan
geldt regel 5: het schip dat **van stuurboord komt** heeft voorrang! Als beide schepen recht
op elkaar af varen, dan moeten ze **beiden naar stuurboord uitwijken**.

### Regel 6: Bakboordzeil
Zijn beide schepen zeilboten? Dan heeft de boot met zijn zeil "**over bakboord**" voorrang.
Het zeil "over bakboord" betekent dat je het grootzeil van je boot aan de linkerkant zit.
Hebben ze allebei het zeil aan dezelfde kant? Dan geldt de laatste regel!

### Regel 7: Loef wijkt voor lij
Het schip dat lager aan de wind vaart (aan lij) heeft voorrang op het schip dat hoger aan de
wind vaart (loef). Tip: Als je onder je zeil door moet kijken om de andere boot te zien ben je
"loef"; anders ben je "lij".
