---
title: Hogerwal afvaren
type: docs
---

{{< interactive w=450 h=800 >}}
{{< interactive_line x=0 y=150 l=450 >}}
{{< interactive_img i="wind.svg" x=10 y=10 w=70 h=120 >}}
{{< interactive_img i="zeilboot-killend.svg" x=125 y=150 w=30 h=80 >}}
{{< interactive_label x=120 y=200 label="1." >}}
{{< interactive_img i="zeilboot-killend.svg" x=125 y=250 w=30 h=80 >}}
{{< interactive_label x=120 y=300 label="2." >}}
{{< interactive_img i="zeilboot-killend-fok-bak.svg" x=240 y=340 w=30 h=80 r=35 >}}
{{< interactive_label x=215 y=380 label="3." >}}
{{< interactive_img i="zeilboot-halve-wind.svg" x=300 y=500 w=30 h=80 r=-90 fx=1 >}}
{{< interactive_label x=280 y=510 label="4." >}}
{{< /interactive >}}

Beslis vooraf over welke boeg je gaat afvaren. Als de wind niet loodrecht vanaf de wal komt is heeft de boeg met de grootste hoek ten opzichte van de wind de voorkeur.

1. De roerganger gaat alvast zitten en zorgt dat het roer recht blijft. Zorg dat iedereen aan boord weet wat er van hem of haar verwacht wordt.
2. Controleer of de weg vrij is voordat je gaat. Zet af van de wal en geef eventueel alvast een zetje in de goede richting.
3. Trek de *fok bak*, maar houd het grootzeil nog los zolang je nog geen vrije koers vooruit hebt.
4. Zodra er ver genoeg is afgevallen kan de *fok over* en mogen de zeilen aan!

### Tips

* Als er weinig ruimte rond de wal is, is het handig om eerst een stuk te *deinzen* (= achteruit varen) totdat meer ruimte om te draaien is.
