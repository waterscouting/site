---
title: Hogerwal aanleggen
type: docs
---

{{< interactive w=450 h=800 >}}
{{< interactive_line x=0 y=150 l=450 >}}
{{< interactive_line x=70 y=195 l=450 r=45 s=dashed >}}
{{< interactive_img i="wind.svg" x=10 y=10 w=70 h=120 >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=170 y=580 w=30 h=80 r=-45 fx=1 >}}
{{< interactive_label x=165 y=630 label="1." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=370 y=380 w=30 h=80 r=-45 fx=1 >}}
{{< interactive_label x=365 y=430 label="2." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=290 y=340 w=30 h=80 r=-45 >}}
{{< interactive_label x=290 y=390 label="3." >}}
{{< interactive_img i="zeilboot-hoog-aan-de-wind-fok-killend.svg" x=190 y=290 w=30 h=80 r=-45 >}}
{{< interactive_label x=190 y=340 label="4." >}}
{{< interactive_img i="zeilboot-killend.svg" x=100 y=200 w=30 h=80 r=-45 >}}
{{< interactive_label x=100 y=250 label="5." >}}
{{< interactive_img i="zeilboot-killend.svg" x=30 y=150 w=30 h=80 >}}
{{< interactive_label x=20 y=200 label="6." >}}
{{< /interactive >}}

Bepaal vooraf waar je gaat aanleggen. Wacht op het juiste moment als het druk is. Zorg dat landvasten klaarliggen, en dat de bemanning weet wat het plan is. 

1. Begin met opkruisen richting de gekozen aanlegplek.
2. Zodra de aanlegplek *achterlijker dan dwars* ligt ga je overstag richting de aanlegplek.
3. Vaar op een zo hoog mogelijk *aan de wind*se koers naar de gekozen plek.
4. Als de gekozen plek nog te veel aan lei ligt is het risico dat je de snelheid niet goed kan regelen. Val dan een beetje af, en loef daarna weer iets op. Je verliest dan een beetje hoogte, waardoor je op een hogere koers aan komt varen.
5. Snelheid regelen doe je door de zeilen te laten vieren. Je begint met het vieren van de fok, daarna het grootzeil. Eventueel kun je het grootzeil weer iets aantrekken als je al op een te vroeg moment te veel snelheid hebt verloren. 
6. Zorg dat je rustig aankomt varen bij de plek. Een persoon stapt van boord, en legt de boot aan.

### Tips
* Hier wordt de *sliplanding* beschreven, dat is de veiligste methode omdat daarbij de snelheid goed te regelen is. Een andere methode is de *opschieter*.
* Kom je toch met teveel snelheid op de kant af? Val dan af, vaar weg van de kant en probeer het nog een keer. Probeer het liever een keer extra dan dat je te hard de kant op komt.
* Hogerwal aanleggen oefenen kan het beste op een boei, mocht je dan toch te hard gaan dan kan je langs de boei varen.
