---
title: Man overboord
type: docs
---

{{< interactive w=450 h=800 >}}
{{< interactive_img i="wind.svg" x=10 y=10 w=70 h=120 >}}
{{< interactive_img i="drenkeling.svg" x=100 y=210 w=50 h=30 >}}
{{< interactive_line x=70 y=205 l=450 r=45 s=dashed >}}
{{< interactive_img i="zeilboot-halve-wind.svg" x=170 y=200 w=30 h=80 r=-90 fx=1 >}}
{{< interactive_label x=160 y=205 label="1." >}}
{{< interactive_img i="zeilboot-voor-de-wind.svg" x=210 y=280 w=48 h=80 r=180 fx=1 >}}
{{< interactive_label x=230 y=280 label="2." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=250 y=490 w=30 h=80 r=-45 fx=1 >}}
{{< interactive_label x=230 y=510 label="3." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=350 y=390 w=30 h=80 r=-45 fx=1 >}}
{{< interactive_label x=330 y=410 label="4." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=140 y=250 w=30 h=80 r=-45 >}}
{{< interactive_label x=160 y=280 label="5." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=40 y=150 w=30 h=80 r=-45 >}}
{{< interactive_label x=60 y=180 label="6." >}}

{{< /interactive >}}

1. **Roep**: "Man overboord", zodat de bemanning door heeft wat er aan de hand is. **Roep**: "Zwem" naar de drenkeling en gooi (indien mogelijk) iets drijvends. Zorg dat 1 persoon uit de boot de drenkeling aan blijft wijzen.
2. Val zo snel mogelijk af naar *voor de wind*. Pas op voor een onverwachte gijp.
3. Na ongeveer 4 bootlengtes: loef op tot *aan de wind*.
4. Voer een dwarspeiling uit op de drenkeling, zodra deze *achterlijker dan dwars* is ga je overstag. Vaar dan *aan de wind* richting de drenkeling. Uiteindelijk moet deze aan de loefzijde van de boot binnengehaald worden, maar waarschijnlijk verlijert de boot nog iets.
5. Regel de snelheid met de zeilen, op dezelfde manier als bij hogerwal aanleggen. Bepaal wie de drenkeling binnenhaalt, en doe dat aan de loefzijde van de boot, direct achter de zijstag. Diegene **roept** "man vast" als dat gebeurd is.
6. Haal de drenkeling zo horizontaal mogelijk binnen en verleen EHBO, ga eventueel *bijliggen*.

### Tips
* Als je oefent door een stootwil als drenkeling te gebruiken, zorg dan dat deze verzwaard is. Anders drijft de stootwil te snel af op de wind, en dat zou bij een persoon niet snel gebeuren.
* Zorg dat je eerst hogerwal aanleggen onder de knie hebt voordat je de man-overboord-manoeuvre oefent.
* Probeer het draaien in de manoeuvre zo snel mogelijk te doen, dan verlies je de minste afstand ten opzichte van de drenkeling.
