---
title: Gijp
type: docs
---

{{< interactive w=450 h=800 >}}
{{< interactive_img i="wind.svg" x=10 y=10 w=70 h=120 >}}
{{< interactive_img i="zeilboot-voor-de-wind-fok-te-loevert.svg" x=200 y=60 w=55 h=80 r=190 >}}
{{< interactive_label x=200 y=70 label="1." >}}
{{< interactive_img i="zeilboot-voor-de-wind-fok-te-loevert.svg" x=190 y=210 w=55 h=80 r=180 >}}
{{< interactive_label x=190 y=220 label="2." >}}
{{< interactive_img i="zeilboot-voor-de-wind.svg" x=180 y=360 w=50 h=80 r=180 fx=1 >}}
{{< interactive_label x=190 y=370 label="3." >}}
{{< interactive_img i="zeilboot-voor-de-wind.svg" x=190 y=510 w=50 h=80 r=190 fx=1 >}}
{{< interactive_label x=200 y=520 label="4." >}}
{{< /interactive >}}

Zorg dat je goed *voor de wind* vaart. Anders moet je eerst afvallen tot *voor de wind* voordat je een gijp doet.

1. Ga vast aan de andere kant van de helmstok zitten (straks de loefzijde), maar blijf met je gezicht naar voren kijken.
2. Ga iets *binnen de wind* varen door iets verder af te vallen zonder dat je al gijpt. Let op: je zit al aan de andere kant van de helmstok. Houd daarna een rechte koers aan en geef aan dat er gegijpt gaat worden. **Commando**: "klaar voor de gijp".
3. Klem je helmstok vast tussen je arm en lichaam. **Commando**: "gijp"! Haal de grootschoot snel binnen, laat het grootzeil over komen en vier de grootschoot snel weer. Ondertussen houd je een zo recht mogelijke koers.
4. Zodra de gijp voltooid is kan je van koers veranderen en oploeven.

### Tips

* Dit is de zogenaamde "rechte gijp", er is ook nog een "S-gijp". De verschillen in uitvoering zijn klein, maar de rechte gijp is veiliger bij harde wind.
* Oefen vooral het herkennen van wanneer je op de goede koers vaart (het herkennen van een gijp) en het snel binnenhalen en vieren van de grootschoot.

### Stormrondje
Als de omstandigheden niet geschikt zijn voor een gijp, bijvoorbeeld bij harde wind, overweeg dan een stormrondje te maken:

1. Loef rustig op tot een *aan de wind*se koers.
2. Voer dan een normale overstag uit.
3. Na de overstag kan je snel afvallen, houd de *fok bak* tot kort voordat de gewenste koers wordt bereikt.
