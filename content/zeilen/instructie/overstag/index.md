---
title: Overstag
type: docs
---

{{< interactive w=450 h=800 >}}
{{< interactive_img i="wind.svg" x=10 y=10 w=70 h=120 >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=250 y=640 w=40 h=80 r=-45 >}}
{{< interactive_label x=290 y=675 label="1." >}}
{{< interactive_img i="zeilboot-hoog-aan-de-wind-fok-killend.svg" x=130 y=500 w=40 h=80 r=-30 >}}
{{< interactive_label x=170 y=550 label="2." >}}
{{< interactive_img i="zeilboot-in-de-wind-fok-bak.svg" x=80 y=350 w=40 h=80 r=0 >}}
{{< interactive_label x=100 y=415 label="3." >}}
{{< interactive_img i="zeilboot-hoog-aan-de-wind-fok-killend.svg" x=130 y=200 w=40 h=80 r=-30 fx=1 >}}
{{< interactive_label x=105 y=250 label="4." >}}
{{< interactive_img i="zeilboot-aan-de-wind.svg" x=250 y=60 w=40 h=80 r=-45 fx=1 >}}
{{< interactive_label x=220 y=100 label="5." >}}
{{< /interactive >}}

Zorg dat je eerst *aan de wind* vaart, anders moet je eerst oploeven tot *aan de wind* voordat je aan de overstag begint.

1. Kijk of er ruimte is om overstag te gaan, andere boten hebben voorrang op je. Zorg dat de bemanning weet wat er gaat gebeuren. **Commando**: "klaar om te wenden".
2. **Commando**: "ree". Dat is het sein aan de fokkenmaat om de fok te vieren. Trek het grootzeil aan, gebruik het roer zo min mogelijk.
3. Als de boot niet uit zichzelf al helemaal door de wind draait: trek de *fok bak* vanaf het moment dat boot recht in de wind ligt. **Commando**: "fok bak". Ga alvast aan de andere kant (straks de loefzijde) van de helmstok zitten.
4. Zodra de voorpunt van de boot goed door de wind draait kan de fok over naar de andere kant. **Commando**: "fok over". Laat ook het grootzeil weer iets vieren, waardoor je nu afvalt over de nieuwe boeg.
5. Als het grootzeil wind begint te vangen over de nieuwe boeg mag de fok weer aan. **Commando**: "fok aan"

### Tips

* Gebruik het roer zo min mogelijk, dan verlies je minder snelheid en hoogte.
* Kom je niet goed overstag? Vaak vaar je dan al iets te hoog aan de wind en in de "dode hoek". Val wat af, bouw een beetje snelheid op en probeer het opnieuw.
