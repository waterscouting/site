---
title: Termen
type: docs
weight: 1
description: >
  Hoe noemen we dingen in een zeilboot?
---

Op het water worden veel verschillende termen gebruikt, vooral bij zeilboten. Vaak worden namen gegeven aan dingen ten opzichte van de wind. Dit is om soms dingen makkelijk aan te duiden.

### Bakboord en stuurboord
Bij een boot spreek je namelijk niet over links en rechts, maar over *bakboord* en *stuurboord*. Als je op het achterdek van een boot staat en je kijkt naar de voorpunt, dan noemen we de linkerkant van de boot *bakboord*, ook wel afgekort als *BB*. De rechterkant van de boot noemen we *stuurboord* of *SB*. Voor deze termen redeneer je altijd alsof je naar voren kijkt. Stel dat je op het voordek staat, en je kijkt richting het achterdek, dan noemen we de linkerkant van de boot dus *stuurboord*. Dat maakt het makkelijk om te praten over de verschillende kanten. Het maakt dan namelijk niet uit waar je staat.

### Loef en lij

Loef en lij zijn nog twee manieren om kanten aan te duiden, maar die zijn afhankelijk waar de wind vandaan komt. De kant van de boot waar de wind vandaan komt noemen we *loef*, de kant waar de wind naartoe blaast noemen we *lij*. Loef noemen we soms ook wel de *hoge kant*, en lij de *lage kant*. Je kan dit het beste onthouden door te denken dat wind van hoog naar laag waait.

### Hogerwal, lagerwal, langswal

De grens tussen water en land noemen we een *wal*, en ook daar zijn er verschillende namen voor afhankelijk waar de wind vandaan komt. De wal waar de wind vandaag komt noemen we de *hogerwal*. De wal waar de wind naar toe waait noemen we de *lagerwal*. Er is nog een derde mogelijkheid, namelijk dat de wind dwars op de wal staat, de wind waait er als het ware langs. Dit noemen we natuurlijk een *langswal*.
