---
title: Zeilkoersen
type: docs
weight: 2
description: >
  Welke koersen bestaan er in een zeilboot?
---

Een zeilboot kan bijna alle kanten op varen, maar niet recht tegen de wind in. Elke koers heeft zijn eigen naam en bijbehorende stand van de zeilen. Als je voor de wind vaart kunnen de zeilen heel erg los, en hoe verder je tegen de wind in gaat varen hoe strakker te zeilen moeten.

Dit zijn de zeilkoersen:

* **In de wind**

  In de wind is eigenlijk geen zeilkoers. Bij deze koers ligt de *steven* van de boot recht in de wind. Op deze koers kan je niet zeilen. Toch spreken we van *in de wind* als je bijvoorbeeld voor anker ligt of aangelegd.

* **Hoog aan de wind**

  Hoog aan de wind is bijna hetzelfde als *aan de wind*, maar de focus ligt er op dat je zo hoog mogelijk als de boot kan vaart. Bij een lelievlet is dat bijna hetzelfde als aan de wind.

* **Aan de wind**

  Bij aan de wind vaar je onder een hoek van ongeveer 45 graden ten opzichte van in de wind. Het is een belangrijke koers omdat je hiermee toch naar punten kunt zeilen die in de wind liggen, hoewel dat dan niet in een rechte lijn is. Op deze zeilkoers heb je de zeilen strak staan.
  
* **Halve wind**

  Bij halve wind ligt de boot dwars op de wind. De zeilen staat hier wat losser dan bij aan de wind.
  
* **Ruime wind**

  Bij ruime wind komt de wind schuin van achteren. De zeilen kunnen al veel losser, maar ze moeten wel wind blijven vangen.
  
* **Voor de wind**

  Bij voor de wind komt de wind recht van achteren. Het grootzeil mag nu bijna helemaal los, maar zorg dat deze vrij blijft van de stag. Als je goed recht voor de wind vaart mag de fok naar de andere kant van de boot. Dat heet de *fok te loevert* zetten.
  
* **Binnen de wind**

  Binnen de wind is een beetje een speciale zeilkoers, eentje die je eigenlijk niet wilt varen. Je bent dan verder afgevallen dan *voor de wind*, waardoor er elk moment een gijp kan gebeuren. In feite vaar je bijna ruime wind, maar dan met de zeilen aan de verkeerde kant. Pas op voor een mogelijke (klap)gijp!

### Oploeven en afvallen
Hierboven staan de verschillende zeilkoersen beschreven. De veranderingen tussen zeilkoersen geven we aan met twee bekende termen: oploeven en afvallen.

Bij *oploeven* draait de steven van de boot richting de wind. Zodra de boot met de steven in de wind ligt (zie hierboven) dan kun je niet verder oploeven en dan ga je overstag. *Afvallen* is de andere kant op, de steven van de boot draait dan van de wind af. Zodra je verder gaat afvallen als je voor de wind vaart dan maak je een gijp.

### Opkruisen of laveren
Wanneer een punt waar je naar toe wilt *in de wind* ligt, kan je daar niet zomaar komen. Dat kan wel door te gaan *opkruisen* of *laveren*. Je zeilt hierbij aan de wind afwisseld over bakboord en stuurboord, waarbij je steeds overstag gaat als je dicht bij de kant komt.

