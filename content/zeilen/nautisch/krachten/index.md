---
title: Krachten
type: docs
weight: 3
description: >
  Welke krachten werken er op een zeilboot?
---

### Killen
Het killen van de zeilen kan op twee manieren gebeuren:
1. Je laat je zeilen te ver vieren. Hierdoor zal als eerste bij de mast het zeil (het voorlijk)
gaan tegenbollen, dit noemen ze ook wel het killen van het zeil.
2. Je loeft te ver op (naar de wind toe draaien). Hierdoor vangen de zeilen geen wind en het
zeil gaat tegen bollen of killen

### Krachten op de zeilboot
Het grootzeil en fok hebben ieder een eigen kracht op de boot. Als bijvoorbeeld alleen het grootzeil aangetrokken wordt en de fok wordt losgelaten waait de wind alleen in het grootzeil, hierdoor draait de punt van de boot naar de wind toe. Deze manier gebruik je ook bij het overstag gaan, bij overstag gaan moet de boot door de wind draaien, dit wordt makkelijker als de fok wordt losgelaten
en het grootzeil wordt aangetrokken. Als alleen de fok aangetrokken wordt en het grootzeil wordt
losgelaten, komt er alleen kracht op het voorste gedeelte van de boot en hierdoor draait de punt van de boot van de wind af.
