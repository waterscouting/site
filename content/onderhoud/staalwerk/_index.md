---
title: Staalwerk
type: docs
weight: 2
description: >
  Hoe behandel je het staalwerk van een boot?
---

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="700" height="400" type="text/html" src="https://www.youtube.com/embed/7N_eNgcoLio?autoplay=0&fs=1&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0"></iframe>
