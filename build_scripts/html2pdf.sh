#!/bin/bash
source="$1"
dest="${source/build/static}"
dest_dir=$(dirname "${dest}")
dest="${dest_dir}.pdf"
dest_dir=$(dirname "${dest}")
echo "Building HTML source: $source"
echo "To PDF file: $dest"
mkdir -p $dest_dir
wkhtmltopdf --print-media-type --footer-center "waterscouting.net" -O Landscape $source $dest
